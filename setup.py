import setuptools
import classattr

setuptools.setup(
    name = 'classattr',
    version = '1.0.5',
    author = 'Make for art and science - Thierry Dassé',
    url = 'https://framagit.org/makeforartandscience/classattr',
    license = 'MIT License',
    description = 'classattr provides tools to manage class and object attributes',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    packages=['classattr'],
    install_requires=[],
    classifiers = [
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
    ],
)
